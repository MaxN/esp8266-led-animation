#include <PxMatrix.h>
#include <Ticker.h>
#include "FS.h"

// Pins for LED MATRIX
#define P_LAT 16
#define P_A 5
#define P_B 4
#define P_C 15
#define P_OE 2
#define P_D 12
#define P_E 0



#define FRAME_HEIGHT 32 
#define FRAME_WIDTH 64

#define RGB565 true
#if RGB565
  #define FRAME_SIZE 4096//(FRAME_HEIGHT * FRAME_WIDTH * 2)
#else
  #define FRAME_SIZE 6144//(FRAME_HEIGHT * FRAME_WIDTH * 3)
#endif

//variables

PxMATRIX display(64, 32, P_LAT, P_OE, P_A, P_B, P_C, P_D, P_E);

Ticker display_ticker;

String filenames[100];
int no_files=0;
int current_file = 0;
uint8_t buffer[FRAME_SIZE] = {0};
unsigned long anim_time = 0;
File ff;

//frame variables
int frames_count = 0;
int current_frame = 0;


// Some standard colors
uint16_t myRED = display.color565(255, 0, 0);
uint16_t myGREEN = display.color565(0, 255, 0);
uint16_t myBLUE = display.color565(0, 0, 255);
uint16_t myWHITE = display.color565(255, 255, 255);
uint16_t myYELLOW = display.color565(255, 255, 0);
uint16_t myCYAN = display.color565(0, 255, 255);
uint16_t myMAGENTA = display.color565(255, 0, 255);
uint16_t myBLACK = display.color565(0, 0, 0);


// ISR for display refresh
void display_updater() {
  display.display(10);
}

void showFiles() {
  SPIFFS.begin();
  Serial.println("showing files:");
  Dir dir = SPIFFS.openDir("/");
  while (dir.next()) {

    Serial.println(dir.fileName());
    filenames[no_files]=dir.fileName();
    no_files++;
  }
}
void openFile(int index) {
  Serial.println("Openning file " + filenames[index]);
  ff = SPIFFS.open(filenames[index], "r");
  if (!ff) {
    Serial.println("file open failed" );
    return;
  }
  Serial.println("File size = " + String(ff.size()));
  frames_count = ff.size() / FRAME_SIZE;
  current_frame = 0;
  Serial.println("FRAME_SIZE = " + String(FRAME_SIZE));
  Serial.println("frame count = "+ String(frames_count));
  Serial.println("opened successfully!!!");
  
}
bool readFile() {
  

//  for (int this_byte=0;this_byte<FRAME_SIZE;this_byte++){
//    ff.read(buffer+this_byte, 1);
//  }
  ff.read(buffer, FRAME_SIZE);
  
  current_frame++;
  if (current_frame == frames_count) {
//    Serial.println("Read all frames. Seek to start of file");
//    ff.seek(0, SeekSet);
//    current_frame = 0;
    return true;
  }
  Serial.println("read frame done. ");
  return false;
}


void drawFrame() {
  int i = 0;
  for(int y = 0; y<FRAME_HEIGHT; y++)
    for(int x = 0; x<FRAME_WIDTH; x++) {
      #if RGB565
      uint16_t  color = buffer[i+1] << 8 | buffer[i];
      display.drawPixelRGB565(x, y, color);
      i = i + 2;
      #else
      display.drawPixelRGB888(x, y, buffer[i], buffer[i + 1], buffer[i + 2]);
      i = i + 3;
      #endif
    }
  
}
void setup() {
  Serial.begin(115200);
  showFiles();
  display.begin(16);
  display.clearDisplay();
  display_ticker.attach(0.002, display_updater);
  display.setTextColor(myCYAN);
  display.setCursor(2,0);
  display.print("Hello World!");
  yield();
  delay(1000);
  openFile(0);
  
 

}

void loop() {
  // put your main code here, to run repeatedly:
  if (millis() - anim_time > 150) {
    Serial.println("Frame + " + String(current_frame));
    anim_time = millis();
    bool done = readFile();
    drawFrame();
    if (done) {
      Serial.println("Read next file");
      current_file++;
      if (current_file == no_files) {
        current_file = 0;
        Serial.print("Read all files. Go to first one");
      }
        
      openFile(current_file);
      
    }
    
  }
  


}
